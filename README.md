# SDK-Rebuilds

Rebuild Android(tm) SDK from source
===================================

- You can build all SDKs up to Android 10 (api level 29) from the build scripts found [here](https://gitlab.com/android-rebuilds/auto). For Android 12 (api 31), 12L (api 32), 13 (api 33), 14 (api 34) and 15 (api 35) you can use the build scripts from this repository. For Android 11 (api 30), the build-recipe should be very similiar (if not identical) to Android 12 (api 31), but this was not tested.

- For the sake of simplicity, the build recipes here *do **not** use docker*. 

- All scripts have been written from scratch and support a build-chain on a modern, up-to-date OS (Linux Mint 21).

- If you do not want to mess up your main production machine, use a VM. 

- This repository includes the build recipes in the corresponding folders.

- The build scripts have been tested on Linux Mint 21. 

- Currently, the build environment (packages necessary) is identical for Android 12 (api 31), 12L (api 32), 13 (api 33), 14 (api 34) and 15 (api 35).

- the scripts for the sdk api 31-34 primarily build the linux sdk; some of them have built the Windows sdk files sucessfully, but this has not been tested for all the sdks. However, the other ones *should* built, too. The script for api 35 cross-compiles for Windows and produces both the linux and Windows sdk files (see subfolder for more details).

Notice for all sdks from api 31 onward
--------------------------------------

The recipes for api 31 and onward do NOT build a system image; should you need one, you need to build it seperately.

Prerequisites
=============

You need approx. 160 GB of free disk space for the source code and the output files generated during the build process. I recommend to have at least 300 GB of free disk space available.

The number of files, both the source code and the output files together, is remarkably high. My last build (api 35) counted over *two million* files (2 097 509, including directories). Some people reported that they ran out of inodes when building. Make sure that the file system of your choice can handle this number of files. The *df --inodes* command might be your friend. There are reported issues when building on zfs. I did the build on ext4 without any problems regarding the file system and/or the number of files.

I recommend to use a machine with at least 32 GB of RAM. Lower specs might work, but are untested. It is commonly asserted that sdk builds fail when you have less than 16 GB of free RAM. Having (at least) 64 GB of RAM speeds up the build process.

You should build on a (virtual) machine running (either Ubuntu 22.04.1 LTS, *untested* for sdk 34 ond above) Linux Mint 21. 

You have to download a lot of source code, and the 1st build usually take a couple of hours, depending on your machine. So take your time and consider to grab a coffee in the meantime.

How to build
============

Install git:

    sudo apt update
    sudo apt install git

Install repo tool:

    sudo apt install repo

Should your distro not have it, you can get the repo tool from [here](https://android.googlesource.com/tools/repo). It is a single script file, so no compiling is necessary. It needs the python-is-python3 symlink package installed to work, *or* a manually set symlink from 'python' to 'python3'.

Clone **this** repository locally:

    git clone https://codeberg.org/Starfish/Android-SDK-Rebuilds

cd into the local copy and desired sdk folder and follow the instructions in the README.md file there.

Before building, please carefully read the README.md in the corresponding folder first!

Last words
==========

I am an app dev, not a package builder. The time I am willing to spend on rebuilding the sdk is limited. I am lucky when I get the sdk compiled, and that is also the point when I stop any investigations. Take care!

Footnote
========

Microsoft Windows is a trademark of the Microsoft group of companies. Android is a trademark of Google LLC.

