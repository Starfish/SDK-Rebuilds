VERSION='15.0.0_r9'
====================

NOTE
----

This build recipe produces the sdk for linux and cross-compiles it for Windows. So you will get the binaries for both OSes in a single run. You will get seperate zip files for the *build tools* and *platform tools* for linux and Windows. The other parts of the sdk suit both.


IMPORTANT
---------

The Android(tm) 15 sdk (api level 35), just like 14 & 13, does *not* include a system image. This is ok, because you do not really need it (unless you use a VM instead of a real device) for development. 

Should you need the system image(s), you need to build them seperately. This is not covered here, however it is well documented on the aosp pages.

Notes
-----

The *lunch* tool now expects three parameters: *device*-*tree*-*variant*. This changed from previous versions. As a result, the file naming of the sdk is a little bit different to previous versions.

To keep consistent with previous build scripts, the *build.sh* script simply *copies* the sdk to an other .zip file with just an other file name to better indicate the platform version, build variant and sdk revision. You can ignore this file, if you want.

Issues with conscrypt that occured during builds of previous versions seem fixed, so no need for the workarounds applied in 13 & 14.

Revision seven (15.0.0_r7) was also tested and builds with the same requisites.

All commands need to be executed from the sdk folder!

Build recipe
-------------

Make all the .sh scripts in this directory executable:

    chmod +x *.sh

Install dependencies (you will be asked for root password to install packages):

    ./dependencies.sh

Download the source code:

    ./download.sh

Compile the sdk:

    ./build.sh

When the build finishes sucessfully, you get a list of the important generated sdk files.

Have fun! 

Footnote
--------

Microsoft Windows is a trademark of the Microsoft group of companies. Android is a trademark of Google LLC.
