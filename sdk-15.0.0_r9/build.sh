#!/bin/bash

VERSION='15.0.0_r9'

BUILD_VARIANT='eng'
export USER=$(whoami)

source build/envsetup.sh

# lunch has new parameter format
lunch sdk-aosp_current-${BUILD_VARIANT}
m sdk dist sdk_repo

# list the result files
# ---------------------
#
# Contrary to previous builds, this on cross-compiles also for windows

# copy the sdk once to get the same file name formatting like previous versions 
# just for convenience

cp out/dist/android-sdk_linux-x86-${USER}.zip out/dist/android-sdk_${BUILD_VARIANT}.${VERSION}_linux-x86.zip

# list the sdk

ls -lah out/dist/android-sdk_linux-x86-${USER}.zip
ls -lah out/dist/android-sdk_${BUILD_VARIANT}.${VERSION}_linux-x86.zip

# the parts of the sdk

ls -lah out/dist/sdk-repo*.zip
ls -lah out/dist/repo*.xml

